<?php


class Author
{
    public $id;
    public $firstName;
    public $lastName;
    public $grade;

    public function __construct($firstName, $lastName, $grade, $id=null) {
        $this->id = $id;
        $this->grade = $grade;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    public function getAuthorName()
    {
        return $this -> firstName." ".$this -> lastName;
    }
}