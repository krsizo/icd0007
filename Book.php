<?php
require_once "script.php";

class Book
{
    public $id;
    public $title;
    public $grade;
    public $isRead;
    public $author1;

    public function __construct($title, $grade, $isRead, $author1, $id=null) {
        $this->id = $id;
        $this->grade = $grade;
        $this->isRead = $isRead;
        $this->title = $title;
        $this->author1 = $author1;
    }

    function getNameByID() {
        $authorID = $this -> author1;
        $firstname = "";
        $lastname = "";
        $conn = getConnection();
        $stmt = $conn ->prepare("select firstName, lastName from authors where id='$authorID'");
        $stmt -> execute();
        foreach ($stmt as $author) {
            $firstname = isset($author['firstName']) ? $author['firstName'] : " ";
            $lastname = isset($author['lastName']) ? $author['lastName'] : " ";
        }
        return $firstname." ".$lastname;
    }
}