<?php

require_once "script.php";
require_once "vendor/tpl.php";
require_once "Book.php";
require_once "Author.php";

$conn = getConnection();

$id = isset($_POST["id"]) ? $_POST["id"]: " ";
$grade = isset($_POST["grade"]) ? $_POST["grade"] : "0";
$title = isset($_POST["title"]) ? urlencode($_POST["title"]) : " ";
$author1 = isset($_POST["author1"]) ? $_POST["author1"] : 0;
$isRead = isset($_POST["isRead"]) ? 1 : 0;

$error = "";
$invalidtitle = "";

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    if (strlen(urldecode($title)) > 2 and strlen(urldecode($title)) < 24) {
        $error = "";
        $stmt = $conn->prepare("insert into books(title, grade, isRead, author) values ('$title', '$grade', $isRead, $author1)");
        $stmt->execute();
        header("Location: /book-list.php?index=1");
    } elseif (strlen($title) < 3 or strlen($title) > 23) {
        $error = "Error";
        $invalidtitle .= $title;
    }
}


$stmt = $conn ->prepare("select * from authors");
$stmt -> execute();
$authors = [];
foreach ($stmt as $author) {
    $authorID = isset($author['id']) ? $author['id'] : 0;
    $firstName = isset($author['firstName']) ? $author['firstName'] : " ";
    $lastName = isset($author['lastName']) ? $author['lastName'] : " ";
    $authorGrade = isset($author['grade']) ? $author['grade'] : " ";
    array_push($authors, new Author($firstName, $lastName, $authorGrade, $authorID));
}

if ($error == "") {
    $books = new Book($title,$grade,$isRead,$author1,$id);
} else {
    $books = new Book($invalidtitle,$grade,$isRead,$author1,$id);
}

function getAuthorById($authorID) {
    $conn = getConnection();
    $stmt = $conn ->prepare("select id, firstName, lastName, grade from authors where id=$authorID");
    $stmt -> execute();
    $ID = 0;
    $firstName = "";
    $lastName = "";
    $authorGrade = "";
    foreach ($stmt as $author) {
        $ID = isset($author['id']) ? $author['id'] : 0;
        $firstName = isset($author['firstName']) ? $author['firstName'] : " ";
        $lastName = isset($author['lastName']) ? $author['lastName'] : " ";
        $authorGrade = isset($author['grade']) ? $author['grade'] : " ";
    }
    return new Author($firstName, $lastName, $authorGrade, $ID);
}

$data = [
    "error" => $error,
    "books" => $books,
    "author" => getAuthorById($author1),
    "authorsList" => $authors
];

print renderTemplate("book-add.html", $data);