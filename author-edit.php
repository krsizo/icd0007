<?php

require_once "script.php";
require_once "vendor/tpl.php";
require_once "Author.php";

$invalidname = "";
$invalidlastName = "";
$error = "";
$firstName = "";
$lastName = "";
$grade = "";
$id = 0;

if ($_SERVER["REQUEST_METHOD"] === "GET") {
    $conn = getConnection();
    $id = $_GET["id"];
    $stmt = $conn ->prepare("select firstName, lastName, grade from authors where id='$id'");
    $stmt -> execute();
    foreach ($stmt as $author) {
        $firstName = isset($author["firstName"]) ? $author["firstName"] : " ";
        $lastName = isset($author["lastName"]) ? $author["lastName"] : " ";
        $grade = isset($author["grade"]) ? $author["grade"] : " ";
    }
} else {
    $firstName = isset($_POST['firstName']) ? $_POST['firstName'] : " ";
    $lastName = isset($_POST['lastName']) ? $_POST['lastName'] : " ";
    $grade = isset($_POST['grade']) ? $_POST['grade'] : " ";
}


if ($_SERVER["REQUEST_METHOD"] === "POST" and isset($_POST["deleteButton"])?$_POST["deleteButton"]:" " == "Kustuda") {
    $conn = getConnection();
    $id = $_POST["id"];
    $stmt = $conn -> prepare("delete from authors where id='$id'");
    $stmt -> execute();
    header("Location: /author-list.php?index=3");

} elseif ($_SERVER["REQUEST_METHOD"] === "POST" and (strlen($firstName) < 1 or strlen($firstName) > 21 or strlen($lastName) < 2 or strlen($lastName) > 22)) {

    $id = $_POST["id"];
    $firstName = $_POST["firstName"];
    $lastName = $_POST["lastName"];
    $grade = $_POST["grade"];

    $error = "Eesnimi peab olema 1-21 märki ja perekonnanimi 2-22 märki!";
    $invalidname .= $_POST["firstName"];
    $invalidlastName .= $_POST["lastName"];

} elseif ($_SERVER["REQUEST_METHOD"] === "POST") {
    $error = "";
    $id = $_POST["id"];
    $firstName = $_POST["firstName"];
    $lastName = $_POST["lastName"];
    $grade = $_POST["grade"];
    $conn = getConnection();

    $stmt = $conn -> prepare("update authors set firstName='$firstName', lastName='$lastName', grade='$grade' where id='$id'");
    $stmt -> execute();
    header("Location: /author-list.php?index=2");
}

if ($error == "") {
    $author = new Author($firstName, $lastName, $grade, $id);
} else {
    $author =  new Author($invalidname, $invalidlastName, $grade, $id);
}

$data = [
    "error" => $error,
    "author" => $author
];


print renderTemplate("author-edit.html", $data);
