<?php

require_once "script.php";
require_once "vendor/tpl.php";
require_once "Author.php";

$conn = getConnection();

$stmt = $conn ->prepare("select * from authors");
$stmt -> execute();

$authors = [];
foreach ($stmt as $author):
    $id = $author["id"];
    $firstName = $author["firstName"];
    $lastName = $author["lastName"];
    $grade = $author["grade"] ;
    array_push($authors, new Author($firstName, $lastName, $grade, $id));
    endforeach;

$index = isset($_GET["index"]) ? $_GET["index"] : " ";
if ($index == 1) {
    $message = "Autor lisanud";
} elseif ($index == 2) {
    $message = "Autor muudanud";
} elseif ($index == 3) {
    $message = "Autor kustutatud";
} else {
    $message = "";
}

$data = [
    "message" => $message,
    "authors" => $authors
];

print renderTemplate("author-list.html", $data);


