<?php
require_once "vendor/tpl.php";
require_once "Request.php";
require_once "script.php";
require_once "Author.php";
require_once "Book.php";

$request = new Request($_REQUEST);

$data = [];

$conn = getConnection();

$command = $request -> param('command')
    ? $request->param('command')
    : 'show_book_list';

if ($command === 'show_book_form') {

    $id = isset($_POST["id"]) ? $_POST["id"] : " ";
    $grade = isset($_POST["grade"]) ? $_POST["grade"] : "0";
    $title = isset($_POST["title"]) ? urlencode($_POST["title"]) : " ";
    $author1 = isset($_POST["author1"]) ? $_POST["author1"] : 0;
    $isRead = isset($_POST["isRead"]) ? 1 : 0;

    $error = "";
    $invalidtitle = "";

    if ($_SERVER["REQUEST_METHOD"] === "POST") {
        if (strlen(urldecode($title)) > 2 and strlen(urldecode($title)) < 24) {
            $error = "";
            $stmt = $conn->prepare("insert into books(title, grade, isRead, author) values ('$title', '$grade', $isRead, $author1)");
            $stmt->execute();
            header("Location: index.php?command=show_book_list&index=1");
            exit();
        } elseif (strlen($title) < 3 or strlen($title) > 23) {
            $error = "Error";
            $invalidtitle .= $title;
        }
    }


    $stmt = $conn->prepare("select * from authors");
    $stmt->execute();
    $authors = [];
    foreach ($stmt as $author) {
        $authorID = isset($author['id']) ? $author['id'] : 0;
        $firstName = isset($author['firstName']) ? $author['firstName'] : " ";
        $lastName = isset($author['lastName']) ? $author['lastName'] : " ";
        $authorGrade = isset($author['grade']) ? $author['grade'] : " ";
        array_push($authors, new Author($firstName, $lastName, $authorGrade, $authorID));
    }

    if ($error == "") {
        $books = new Book($title, $grade, $isRead, $author1, $id);
    } else {
        $books = new Book($invalidtitle, $grade, $isRead, $author1, $id);
    }

    function getAuthorById($authorID)
    {
        $conn = getConnection();
        $stmt = $conn->prepare("select id, firstName, lastName, grade from authors where id=$authorID");
        $stmt->execute();
        $ID = 0;
        $firstName = "";
        $lastName = "";
        $authorGrade = "";
        foreach ($stmt as $author) {
            $ID = isset($author['id']) ? $author['id'] : 0;
            $firstName = isset($author['firstName']) ? $author['firstName'] : " ";
            $lastName = isset($author['lastName']) ? $author['lastName'] : " ";
            $authorGrade = isset($author['grade']) ? $author['grade'] : " ";
        }
        return new Author($firstName, $lastName, $authorGrade, $ID);
    }

    $data = [
        "error" => $error,
        "books" => $books,
        "author" => getAuthorById($author1),
        "authorsList" => $authors,
        "command" => "show_book_form"
    ];

    print renderTemplate("book-add.html", $data);

} else if ($command === 'show_book_list'){

    $stmt = $conn->prepare("select * from books");
    $stmt->execute();
    $authorName = "";
    $books = [];
    foreach ($stmt as $book) {
        $id = $book["id"];
        $title = urldecode($book["title"]);
        $grade = $book["grade"];
        $author = $book["author"];
        $isRead = $book["isRead"];

        array_push($books, new Book($title, $grade, $isRead, $author, $id));
    }


    $index = isset($_GET["index"]) ? $_GET["index"] : " ";
    if ($index == 1) {
        $message = "Raamat lisanud";
    } elseif ($index == 2) {
        $message = "Raamat muudanud";
    } elseif ($index == 3) {
        $message = "Raamat kustutatud";
    } else {
        $message = "";
    }


    $data = [
        "message" => $message,
        "books" => $books,
        "command" => "show_book_list"
    ];

    print renderTemplate("book-list.html", $data);

} else if ($command === 'show_author_form'){


    $id = isset($_POST["id"]) ? $_POST["id"] : " ";
    $firstName = isset($_POST["firstName"]) ? $_POST["firstName"] : " ";
    $lastName = isset($_POST["lastName"]) ? $_POST["lastName"] : " ";
    $grade = isset($_POST["grade"]) ? $_POST["grade"] : " ";
    $submitButton = isset($_POST["submitButton"]) ? $_POST["submitButton"] : " ";

    $invalidname = "";
    $invalidlastName = "";
    $error = "";

    if ($_SERVER["REQUEST_METHOD"] === "POST" and strlen($firstName) > 0 and strlen($firstName) < 22 and strlen($lastName) > 1 and strlen($lastName) < 23) {
        $error = "";
        $stmt = $conn->prepare("insert into authors (firstName, lastName, grade) values ('$firstName', '$lastName', '$grade')");
        $stmt->execute();
        header("Location: index.php?command=show_author_list&index=1");
        exit();
    } elseif ($_SERVER["REQUEST_METHOD"] === "POST" and (strlen($firstName) < 1 or strlen($firstName) > 21 or strlen($lastName) < 2 or strlen($lastName) > 22)) {
        $error = "Eesnimi peab olema 1-21 märki ja perekonnanimi 2-22 märki!";
        $invalidname .= $firstName;
        $invalidlastName .= $lastName;
    }


    if ($error == "") {
        $author = new Author($firstName, $lastName, $grade, $id);
    } else {
        $author = new Author($invalidname, $invalidlastName, $grade, $id);
    }

    $data = [
        "error" => $error,
        "author" => $author,
        "command" => "show_author_form"
    ];


    print renderTemplate("author-add.html", $data);

} else if ($command === 'show_author_list') {


    $stmt = $conn->prepare("select * from authors");
    $stmt->execute();

    $authors = [];
    foreach ($stmt as $author):
        $id = $author["id"];
        $firstName = $author["firstName"];
        $lastName = $author["lastName"];
        $grade = $author["grade"];
        array_push($authors, new Author($firstName, $lastName, $grade, $id));
    endforeach;

    $index = isset($_GET["index"]) ? $_GET["index"] : " ";
    if ($index == 1) {
        $message = "Autor lisanud";
    } elseif ($index == 2) {
        $message = "Autor muudanud";
    } elseif ($index == 3) {
        $message = "Autor kustutatud";
    } else {
        $message = "";
    }

    $data = [
        "message" => $message,
        "authors" => $authors,
        "command" => "show_author_list"
    ];

    print renderTemplate("author-list.html", $data);

} elseif($command === 'show_book_edit') {

    $title = "";
    $grade = "";
    $isRead = "";
    $author1 = 0;
    $id = 0;

    if ($_SERVER["REQUEST_METHOD"] === "GET") {
        $conn = getConnection();
        $id = $_GET["id"];
        $stmt = $conn->prepare("select title, grade, isRead, author from books where id='$id'");

        $stmt->execute();
        foreach ($stmt as $book) {
            $title = isset($book["title"]) ? urldecode($book["title"]) : "";
            $grade = isset($book["grade"]) ? $book["grade"] : "0";
            $isRead = isset($book["isRead"]) ? $book["isRead"] : "0";
            $author1 = isset($book["author"]) ? $book["author"] : 0;
        }
    } else {
        $id = isset($_POST["id"]) ? $_POST["id"] : 0;
        $title = isset($_POST["title"]) ? urldecode($_POST["title"]) : "";
        $author1 = isset($_POST["author1"]) ? $_POST["author1"] : 0;
        $grade = isset($_POST["grade"]) ? $_POST["grade"] : "0";
        $isRead = isset($_POST["isRead"]) ? $_POST["isRead"] : "0";
    }

    $invalidtitle = "";
    $error = "";


    if ($_SERVER["REQUEST_METHOD"] === "POST" and isset($_POST["deleteButton"]) ? $_POST["deleteButton"] : " " == "Kustuda") {
        $conn = getConnection();
        $id = $_POST["id"];
        $stmt = $conn->prepare("delete from books where id='$id'");
        $stmt->execute();
        header("Location: /index.php?command=show_book_list&index=3");
        exit();

    } elseif ($_SERVER["REQUEST_METHOD"] === "POST" and (strlen($title) < 3 or strlen($title) > 23)) {

        $id = isset($_POST["id"]) ? $_POST["id"] : 0;
        $title = isset($_POST["title"]) ? urldecode($_POST["title"]) : "";
        $author1 = isset($_POST["author1"]) ? $_POST["author1"] : 0;
        $grade = isset($_POST["grade"]) ? $_POST["grade"] : "0";
        $isRead = isset($_POST["isRead"]) ? $_POST["isRead"] : "0";

        $error = "error";
        $invalidtitle .= $_POST["title"];

    } elseif ($_SERVER["REQUEST_METHOD"] === "POST") {

        $id = isset($_POST["id"]) ? $_POST["id"] : 0;
        $title = isset($_POST["title"]) ? urldecode($_POST["title"]) : "";
        $author1 = isset($_POST["author1"]) ? $_POST["author1"] : 0;
        $grade = isset($_POST["grade"]) ? $_POST["grade"] : "0";
        $isRead = isset($_POST["isRead"]) ? $_POST["isRead"] : "0";

        print_r($_POST);


        $error = "";
        $conn = getConnection();
        $stmt = $conn->prepare("update books set title='$title', grade='$grade', isRead=$isRead, author=$author1 where id='$id'");
        $stmt->execute();
        header("Location: /index.php?command=show_book_list&index=2");
        exit();
    }

    $conn = getConnection();
    $stmt = $conn->prepare("select * from authors");
    $stmt->execute();
    $authors = [];

    foreach ($stmt as $author) {
        $authorid = $author["id"];
        $firstName = $author["firstName"];
        $lastName = $author["lastName"];
        $authorGrade = $author["grade"];
        array_push($authors, new Author($firstName, $lastName, $authorGrade, $authorid));
    }

    function getAuthorById($authorID)
    {
        $conn = getConnection();
        $stmt = $conn->prepare("select id, firstName, lastName, grade from authors where id=$authorID");
        $stmt->execute();
        $id = "";
        $firstName = "";
        $lastname = "";
        $grade = "";
        foreach ($stmt as $author) {
            $id = isset($author['id']) ? $author['id'] : " ";
            $firstName = isset($author['firstName']) ? $author['firstName'] : " ";
            $lastname = isset($author['lastName']) ? $author['lastName'] : " ";
            $grade = isset($author['grade']) ? $author['grade'] : " ";
        }
        return new Author($firstName, $lastname, $grade, $id);
    }

    if ($error == "") {
        $books = new Book($title, $grade, $isRead, $author1, $id);
    } else {
        $books = new Book($invalidtitle, $grade, $isRead, $author1, $id);
    }

    $data = [
        "error" => $error,
        "books" => $books,
        "author" => getAuthorById($author1),
        "authorsList" => $authors,
        "command" => "show_book_edit"
    ];

    print renderTemplate("book-edit.html", $data);

} elseif($command === 'show_author_edit') {

    $invalidname = "";
    $invalidlastName = "";
    $error = "";
    $firstName = "";
    $lastName = "";
    $grade = "";
    $id = 0;

    if ($_SERVER["REQUEST_METHOD"] === "GET") {
        $conn = getConnection();
        $id = $_GET["id"];
        $stmt = $conn->prepare("select firstName, lastName, grade from authors where id='$id'");
        $stmt->execute();
        foreach ($stmt as $author) {
            $firstName = isset($author["firstName"]) ? $author["firstName"] : " ";
            $lastName = isset($author["lastName"]) ? $author["lastName"] : " ";
            $grade = isset($author["grade"]) ? $author["grade"] : " ";
        }
    } else {
        $firstName = isset($_POST['firstName']) ? $_POST['firstName'] : " ";
        $lastName = isset($_POST['lastName']) ? $_POST['lastName'] : " ";
        $grade = isset($_POST['grade']) ? $_POST['grade'] : " ";
    }


    if ($_SERVER["REQUEST_METHOD"] === "POST" and isset($_POST["deleteButton"]) ? $_POST["deleteButton"] : " " == "Kustuda") {
        $conn = getConnection();
        $id = $_POST["id"];
        $stmt = $conn->prepare("delete from authors where id='$id'");
        $stmt->execute();
        header("Location: index.php?command=show_author_list&index=3");
        exit();

    } elseif ($_SERVER["REQUEST_METHOD"] === "POST" and (strlen($firstName) < 1 or strlen($firstName) > 21 or strlen($lastName) < 2 or strlen($lastName) > 22)) {

        $id = $_POST["id"];
        $firstName = $_POST["firstName"];
        $lastName = $_POST["lastName"];
        $grade = $_POST["grade"];

        $error = "Eesnimi peab olema 1-21 märki ja perekonnanimi 2-22 märki!";
        $invalidname .= $_POST["firstName"];
        $invalidlastName .= $_POST["lastName"];

    } elseif ($_SERVER["REQUEST_METHOD"] === "POST") {
        $error = "";
        $id = $_POST["id"];
        $firstName = $_POST["firstName"];
        $lastName = $_POST["lastName"];
        $grade = $_POST["grade"];
        $conn = getConnection();

        $stmt = $conn->prepare("update authors set firstName='$firstName', lastName='$lastName', grade='$grade' where id='$id'");
        $stmt->execute();
        header("Location: index.php?command=show_author_list&index=2");
        exit();
    }

    if ($error == "") {
        $author = new Author($firstName, $lastName, $grade, $id);
    } else {
        $author = new Author($invalidname, $invalidlastName, $grade, $id);
    }

    $data = [
        "error" => $error,
        "author" => $author,
        "command" => "show_author_edit"
    ];


    print renderTemplate("author-edit.html", $data);

}
