<?php

require_once "script.php";
require_once "vendor/tpl.php";
require_once "Book.php";

$conn = getConnection();

$stmt = $conn ->prepare("select * from books");
$stmt -> execute();
$authorName = "";
$books = [];
foreach ($stmt as $book) {
    $id = $book["id"];
    $title = urldecode($book["title"]);
    $grade = $book["grade"];
    $author = $book["author"];
    $isRead = $book["isRead"];

    array_push($books, new Book($title, $grade, $isRead, $author, $id));
}


$index = isset($_GET["index"]) ? $_GET["index"] : " ";
if ($index == 1) {
    $message = "Raamat lisanud";
} elseif ($index == 2) {
    $message = "Raamat muudanud";
} elseif ($index == 3) {
    $message = "Raamat kustutatud";
} else {
    $message = "";
}


$data = [
    "message" => $message,
    "books" => $books
];

print renderTemplate("book-list.html", $data);