<?php

require_once "script.php";
require_once "vendor/tpl.php";
require_once "Book.php";
require_once "Author.php";

$title = "";
$grade = "";
$isRead="";
$author1 = 0;
$id = 0;

if ($_SERVER["REQUEST_METHOD"] === "GET") {
    $conn = getConnection();
    $id = $_GET["id"];
    $stmt = $conn ->prepare("select title, grade, isRead, author from books where id='$id'");

    $stmt -> execute();
    foreach ($stmt as $book) {
        $title = isset($book["title"]) ? urldecode($book["title"]) : "";
        $grade = isset($book["grade"]) ? $book["grade"] : "0";
        $isRead = isset($book["isRead"]) ? $book["isRead"] : "0";
        $author1 = isset($book["author"]) ? $book["author"] : 0;
    }
} else {
    $id = isset($_POST["id"]) ? $_POST["id"] : 0;
    $title = isset($_POST["title"]) ? urldecode($_POST["title"]) : "";
    $author1 = isset($_POST["author1"]) ? $_POST["author1"] : 0;
    $grade = isset($_POST["grade"]) ? $_POST["grade"] : "0";
    $isRead = isset($_POST["isRead"]) ? $_POST["isRead"] : "0";
}

$invalidtitle = "";
$error = "";


if ($_SERVER["REQUEST_METHOD"] === "POST" and isset($_POST["deleteButton"]) ? $_POST["deleteButton"]:" " == "Kustuda") {
    $conn = getConnection();
    $id = $_POST["id"];
    $stmt = $conn -> prepare("delete from books where id='$id'");
    $stmt -> execute();
    header("Location: /book-list.php?index=3");

} elseif ($_SERVER["REQUEST_METHOD"] === "POST" and (strlen($title) < 3 or strlen($title) > 23)) {

    $id = isset($_POST["id"]) ? $_POST["id"] : 0;
    $title = isset($_POST["title"]) ? urldecode($_POST["title"]) : "";
    $author1 = isset($_POST["author1"]) ? $_POST["author1"] : 0;
    $grade = isset($_POST["grade"]) ? $_POST["grade"] : "0";
    $isRead = isset($_POST["isRead"]) ? $_POST["isRead"] : "0";

    $error = "error";
    $invalidtitle .= $_POST["title"];

} elseif ($_SERVER["REQUEST_METHOD"] === "POST") {

    $id = isset($_POST["id"]) ? $_POST["id"] : 0;
    $title = isset($_POST["title"]) ? urldecode($_POST["title"]) : "";
    $author1 = isset($_POST["author1"]) ? $_POST["author1"] : 0;
    $grade = isset($_POST["grade"]) ? $_POST["grade"] : "0";
    $isRead = isset($_POST["isRead"]) ? $_POST["isRead"] : "0";

    print_r($_POST);


    $error = "";
    $conn = getConnection();
    $stmt = $conn->prepare("update books set title='$title', grade='$grade', isRead=$isRead, author=$author1 where id='$id'");
    $stmt->execute();
    header("Location: /book-list.php?index=2");
}

$conn = getConnection();
$stmt = $conn ->prepare("select * from authors");
$stmt -> execute();
$authors = [];

foreach ($stmt as $author) {
    $authorid = $author["id"];
    $firstName = $author["firstName"];
    $lastName = $author["lastName"];
    $authorGrade = $author["grade"];
    array_push($authors, new Author($firstName, $lastName, $authorGrade, $authorid));
}

function getAuthorById($authorID) {
    $conn = getConnection();
    $stmt = $conn ->prepare("select id, firstName, lastName, grade from authors where id=$authorID");
    $stmt -> execute();
    $id = "";
    $firstName = "";
    $lastname = "";
    $grade = "";
    foreach ($stmt as $author) {
        $id = isset($author['id']) ? $author['id'] : " ";
        $firstName = isset($author['firstName']) ? $author['firstName'] : " ";
        $lastname = isset($author['lastName']) ? $author['lastName'] : " ";
        $grade = isset($author['grade']) ? $author['grade'] : " ";
    }
    return new Author($firstName, $lastname, $grade, $id);
}

if ($error == "") {
    $books = new Book($title,$grade,$isRead,$author1,$id);
} else {
    $books = new Book($invalidtitle,$grade,$isRead,$author1,$id);
}

$data = [
    "error" => $error,
    "books" => $books,
    "author" => getAuthorById($author1),
    "authorsList" => $authors
];

print renderTemplate("book-edit.html", $data);