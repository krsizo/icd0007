<?php

require_once "vendor/tpl.php";
require_once "script.php";
require_once "Author.php";

$conn = getConnection();


$id = isset($_POST["id"]) ? $_POST["id"] : " ";
$firstName = isset($_POST["firstName"]) ? $_POST["firstName"] : " ";
$lastName = isset($_POST["lastName"]) ? $_POST["lastName"] : " ";
$grade = isset($_POST["grade"]) ? $_POST["grade"] : " ";
$submitButton = isset($_POST["submitButton"]) ? $_POST["submitButton"] : " ";

$invalidname = "";
$invalidlastName = "";
$error = "";

if ($_SERVER["REQUEST_METHOD"] === "POST" and strlen($firstName) > 0 and strlen($firstName) < 22 and strlen($lastName) > 1 and strlen($lastName) < 23) {
    $error = "";
    $stmt = $conn -> prepare("insert into authors (firstName, lastName, grade) values ('$firstName', '$lastName', '$grade')");
    $stmt -> execute();
    header("Location: /author-list.php?index=1");
} elseif ($_SERVER["REQUEST_METHOD"] === "POST" and (strlen($firstName) < 1 or strlen($firstName) > 21 or strlen($lastName) < 2 or strlen($lastName) > 22)) {
    $error = "Eesnimi peab olema 1-21 märki ja perekonnanimi 2-22 märki!";
    $invalidname .= $firstName;
    $invalidlastName .= $lastName;
}


if ($error == "") {
    $author = new Author($firstName, $lastName, $grade, $id);
} else {
    $author =  new Author($invalidname, $invalidlastName, $grade, $id);
}

$data = [
        "error" => $error,
        "author" => $author
];



print renderTemplate("author-add.html", $data);

?>